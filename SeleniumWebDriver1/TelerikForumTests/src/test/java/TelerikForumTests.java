import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.lang.Object;

import java.sql.Timestamp;
import java.util.concurrent.TimeUnit;

public class TelerikForumTests {
    private static String mainUrl = "https://stage-forum.telerikacademy.com/";
    WebDriver driver;
    Timestamp timestamp = new Timestamp(System.currentTimeMillis());

    @BeforeClass
    public static void classInit() {
        System.setProperty("webdriver.chrome.driver", "G:\\JavaEmchon\\A22\\Personal\\22-emil-nikolov\\WebDrivers\\chromedriver.exe");
        System.setProperty("webdriver.gecko.driver", "G:\\JavaEmchon\\A22\\Personal\\22-emil-nikolov\\WebDrivers\\geckodriver.exe");
    }
    @Before
    public void initTest(){
        this.driver = new FirefoxDriver();
        driver.get(mainUrl);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Before
    public void logIn(){
        WebElement logInButton = driver.findElement(By.className("d-button-label"));
        logInButton.click();
        WebElement emailField = driver.findElement(By.id("Email"));
        emailField.click();
        emailField.sendKeys("emchon@abv.bg");
        WebElement passwordField = driver.findElement(By.id("Password"));
        passwordField.sendKeys("p@rola");
        WebElement signInButton = driver.findElement(By.id("next"));
        signInButton.click();
    }

    @Test
    public void A_createTopicWithoutTitle() {
        WebElement newTopicButton = driver.findElement(By.id("create-topic"));
        newTopicButton.click();
        WebElement createTopicButton = driver.findElement(By.xpath("//button[@aria-label='Create Topic']"));
        createTopicButton.click();
        assertElementPresent("//*[ contains(text(), 'Title is required')]");

    }

    @Test
    public void B_createNewTopicAfterLogIn() {
        WebElement newTopicButton = driver.findElement(By.id("create-topic"));
        newTopicButton.click();
        WebElement newTopicTitleInput = driver.findElement(By.id("reply-title"));
        newTopicTitleInput.sendKeys("Selenium Life is better than no Life at all");
        WebElement newTopicBodyInput = driver.findElement(By.xpath("//textarea[@aria-label='Type here. Use Markdown, BBCode, or HTML to format. Drag or paste images.'] "));
        newTopicBodyInput.sendKeys("Domine, non sum dignus ut intres sub tectum meum Sed tantum dic verbo et sanabitur anima mea." + timestamp.getTime());
        WebElement createTopicButton = driver.findElement(By.xpath("//button[@aria-label='Create Topic']"));
        createTopicButton.click();
        Assert.assertTrue("Error! Current URL is:" + driver.getCurrentUrl() +" expected another!" ,driver.getCurrentUrl().contains("https://stage-forum.telerikacademy.com/"));
    }

    @Test
    public void C_AddCommentAndDeleteComment() {
        replyTopicClass();
        WebElement typeComment = driver.findElement(By.xpath("//textarea"));
        typeComment.click();
        typeComment.sendKeys("Gloria tibi, Domine, Qui natus es de Virgine, Cum Patre et Sancto Spiritu!" + timestamp.getTime());
        WebElement addComment = driver.findElement(By.xpath("(//span[contains(.,'Reply')])[last()]"));
        addComment.click();
        waitFor(2000);
        WebElement threeDotsButton = driver.findElement(By.xpath("(//button[@aria-label='show more'])[last()]"));
        threeDotsButton.click();
        waitFor(2000);
        WebElement deleteButton = driver.findElement(By.xpath("(//button[@title='delete this post'])[last()]"));
        deleteButton.click();
        assertElementPresent("(//p[contains(text(),'(post withdrawn by author, will be automatically deleted in 24 hours unless flagged)')])[last()]");
    }

    @Test
    public void D_AddIllegalAttachmentInCommentWithLink() {
        replyTopicClass();
        WebElement addImage = driver.findElement(By.xpath("//button[@title='Upload']"));
        addImage.click();
        waitFor(3000);
        WebElement fromTheWebRadio = driver.findElement(By.xpath("//label[@for='remote']"));
        fromTheWebRadio.click();
        WebElement inputHTTP = driver.findElement(By.xpath("//input[@placeholder='http://example.com/image.png']"));
        inputHTTP.sendKeys("https://1drv.ms/u/s!AqwGawNkWNnLgZUGWEGRa-iasOtrnA?e=3TJVvH" + Keys.ENTER);
        waitFor(3000);
        WebElement sendAttachment = driver.findElement(By.xpath("(//button[@aria-label='Reply'])[last()]"));
        sendAttachment.click();
        waitFor(3000);
        assertElementPresent("//a[@href='https://1drv.ms/u/s!AqwGawNkWNnLgZUGWEGRa-iasOtrnA?e=3TJVvH']");
    }

    private void replyTopicClass() {
        WebElement selectTopic = driver.findElement(By.xpath("(//*[text()[contains(.,'Selenium Life is better than no Life at all')]])[1]"));
        selectTopic.click();
        WebElement replyTopic = driver.findElement(By.xpath("//button[@aria-label='Reply']"));
        replyTopic.click();

    }


    @After
    public void logOutTelerik() {
        driver.close();
    }

    public void assertElementPresent(String locator) {
        Assert.assertNotNull(driver.findElement(By.xpath(locator)));
    }

    public void waitFor(long milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e){
            e.printStackTrace();
        }
    }
}
