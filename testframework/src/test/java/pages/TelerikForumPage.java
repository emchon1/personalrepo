package pages;

import com.telerikacademy.testframework.Utils;
import org.openqa.selenium.WebDriver;

public class TelerikForumPage extends BasePage {
    final String PAGE_URL = Utils.getConfigPropertyByKey("forum.url");

    public TelerikForumPage(WebDriver driver) {
        super(driver);
        super.setUrl(PAGE_URL);
    }

    public void LogIn(){
        actions.waitForElementVisible("forum.LogIn", 10);
        actions.clickElement("forum.LogIn");
        actions.waitForElementVisible("login.email", 10);
        actions.clickElement("login.email");
        actions.typeValueInField("emchon@abv.bg", "login.email");
        actions.clickElement("login.password");
        actions.typeValueInField("p@rola" , "login.password");
        actions.clickElement("login.signup");

    }

    public void AssertSignupPageNavigated(){
        actions.assertNavigatedUrl("forum.url");
    }

    public void AssertProfilePresent(){
        actions.assertElementPresent("login.profile");
    }

    public void AssertElementPresent(String element){
        actions.assertElementPresent(element);
    }
}


