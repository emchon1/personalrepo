package pages;

import com.telerikacademy.testframework.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class GooglePage extends BasePage {
    final String PAGE_URL = Utils.getConfigPropertyByKey("base.url");

    public GooglePage(WebDriver driver) {
        super(driver);
        super.setUrl(PAGE_URL);
    }

    public void openUrl(String url) {
        agreeWithConsent();
        Utils.getWebDriver().get(url);
    }

    public void SearchAndOpenFirstResult(String searchTerm){
        agreeWithConsent();
        actions.waitForElementVisible("search.Input",10);
        actions.typeValueInField(searchTerm, "search.Input");
        actions.waitForElementVisible("search.Button",10);
        actions.clickElement("search.Button");
        actions.waitForElementVisible("search.Result",10);
        actions.clickElement("search.Result");
    }

    public void agreeWithConsent() {
        actions.waitForElementVisible("consent.Iframe",10);
        actions.changeIFrame("consent.Iframe");
        actions.waitForElementVisible("consent.Agree",10);
        actions.clickElement("consent.Agree");
        Utils.getWebDriver().switchTo().defaultContent();
    }

}
