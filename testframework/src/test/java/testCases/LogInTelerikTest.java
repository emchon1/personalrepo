package testCases;

import java.util.concurrent.TimeUnit;

import com.telerikacademy.testframework.Utils;
import org.junit.Before;
import org.junit.Test;
import pages.GooglePage;
import pages.TelerikForumPage;

public class LogInTelerikTest extends BaseTest {
    private String searchCriterion = "https://stage-forum.telerikacademy.com/";

    @Test
    public void logInTelerikForum() throws InterruptedException {
        GooglePage google = new GooglePage(actions.getDriver());

        TelerikForumPage forum = new TelerikForumPage(actions.getDriver());

        google.openUrl(searchCriterion);
        forum.LogIn();
        Utils.getWebDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        forum.AssertProfilePresent();
    }
}