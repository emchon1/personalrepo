package testCases;
import java.util.concurrent.TimeUnit;

import com.telerikacademy.testframework.Utils;
import org.junit.Before;
import org.junit.Test;
import pages.GooglePage;
import pages.TelerikForumPage;
import testCases.BaseTest;
import java.sql.Timestamp;
import java.util.concurrent.TimeUnit;

public class CreateTopicTest extends BaseTest {
    private String searchCriterion= "https://stage-forum.telerikacademy.com/";
    Timestamp timestamp = new Timestamp(System.currentTimeMillis());

    @Test
    public void a_createNewTopicWithoutTitle() throws InterruptedException {
        GooglePage google = new GooglePage(actions.getDriver());
        TelerikForumPage forum = new TelerikForumPage(actions.getDriver());

        google.openUrl(searchCriterion);
        forum.LogIn();
        Utils.getWebDriver().manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        actions.clickElement("topic.newButton");
        actions.clickElement("topic.createButton");
        forum.AssertElementPresent("topic.titleIsRequired");}

    @Test
        public void b_createNewTopic() throws InterruptedException {
        actions.typeValueInField("Domine, non sum dignus" + timestamp.getTime(), "topic.newTopicTitleInput");
        actions.typeValueInField("ut intres sub tectum meum Sed tantum dic verbo et sanabitur anima mea." + timestamp.getTime(), "topic.newTopicBodyInput");
        actions.clickElement("topic.createButton");
//        actions.assertNavigatedUrl("topic.currentURL");
     }
    }

