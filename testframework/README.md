# README

This is test with Sikuli /Jython framework.

Web site: https://stage-forum.telerikacademy.com/
Browser: Google Chrome ver 85.0.4183.83 , (64 bits)

Four test are included:

* create a topic without a title;
* create a topic as a registered user;
* post and delete a comment;
* attach a file to comment with not-image file extension using url.

It is necessary Sikuli to be installed and added to the PATH.
