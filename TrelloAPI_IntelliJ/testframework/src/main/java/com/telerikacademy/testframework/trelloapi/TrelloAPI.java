package com.telerikacademy.testframework.trelloapi;

import com.telerikacademy.testframework.trelloapi.Models.BoardModel;
import com.telerikacademy.testframework.Utils;
import com.telerikacademy.testframework.trelloapi.Models.CardModel;
import com.telerikacademy.testframework.trelloapi.Models.ListModel;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;

import java.util.ArrayList;
import java.util.List;

public class TrelloAPI {
    private String apiKey;
    private String apiToken;
    private String trelloApiUrl;

    public void authenticate(String userNameKey){
        String fullUserKey = "trello.users." + userNameKey;
        trelloApiUrl = Utils.getConfigPropertyByKey("trello.apiUrl");
        apiKey = Utils.getConfigPropertyByKey(fullUserKey + ".apiKey");
        apiToken = Utils.getConfigPropertyByKey(fullUserKey + ".token");
    }

    public BoardModel createBoard(String name, boolean createDefaultLists){
        String createUrl = trelloApiUrl + "/1/boards/?name="+ name + "&key=" + apiKey + "&token=" + apiToken + "&defaultLists=" + createDefaultLists;
        Utils.LOG.info("POST URL: " + createUrl);

        BoardModel responseBoard =
                RestAssured.given()
                        .contentType(ContentType.JSON)
                        .when()
                        .post(createUrl)
                        .then()
                        .statusCode(200)
                        .extract().response()
                        .as(BoardModel.class);

        return responseBoard;
    }

    public ListModel createList(String name, String boardId){
        String createUrl = trelloApiUrl + "/1/boards/" + boardId + "/lists/?name="+ name + "&key=" + apiKey + "&token=" + apiToken;
        Utils.LOG.info("POST URL: " + createUrl);

        ListModel responseList =
                RestAssured.given()
                        .contentType(ContentType.JSON)
                        .when()
                        .post(createUrl)
                        .then()
                        .statusCode(200)
                        .extract().response()
                        .as(ListModel.class);

        return responseList;
    }

    public CardModel createCard(String name, String createdListID){
        String createUrl = trelloApiUrl + "/1/cards/?name="+ name + "&key=" + apiKey + "&token=" + apiToken + "&idList=" + createdListID;
        Utils.LOG.info("POST URL: " + createUrl);

        CardModel responseCard =
                RestAssured.given()
                        .contentType(ContentType.JSON)
                        .when()
                        .post(createUrl)
                        .then()
                        .statusCode(200)
                        .extract().response()
                        .as(CardModel.class);

        return responseCard;
    }

    public List<BoardModel> getAllBoards(){
        return new ArrayList<BoardModel>();
    }
    public BoardModel getBoard(String name){
        return new BoardModel();
    }
    public void deleteBoard(BoardModel createdBoard) {
        String deleteUrl = trelloApiUrl + "/1/boards/" + createdBoard.id + "?key=" + apiKey + "&token=" + apiToken;
        Utils.LOG.info("Deleting at URL: " + deleteUrl);

        RestAssured.given()
                .contentType(ContentType.JSON)
                .when()
                .delete(deleteUrl)
                .then()
                .statusCode(200);
    }
}
