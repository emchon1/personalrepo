package com.telerikacademy.testframework.pages.trello;

import org.openqa.selenium.WebDriver;
import com.telerikacademy.testframework.pages.BasePage;

public abstract class BaseTrelloPage extends BasePage {
    public BaseTrelloPage(WebDriver driver, String pageUrlKey) {
        super(driver, pageUrlKey);
    }
}
