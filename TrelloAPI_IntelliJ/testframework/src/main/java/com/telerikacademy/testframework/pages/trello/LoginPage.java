package com.telerikacademy.testframework.pages.trello;

import com.telerikacademy.testframework.Utils;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class LoginPage extends BaseTrelloPage {

    public LoginPage(WebDriver driver) {
        super(driver, "trello.loginUrl");
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
    }

    public void loginUser(String userKey){
        String username = Utils.getConfigPropertyByKey("trello.users." + userKey + ".username");
        String password = Utils.getConfigPropertyByKey("trello.users." + userKey + ".password");

        navigateToPage();

        actions.waitForElementVisibleUntilTimeout("trello.loginPage.username",3);
        actions.typeValueInField(username, "trello.loginPage.username");

        actions.waitForElementVisible("trello.loginPage.password");
        actions.typeValueInField(password, "trello.loginPage.password");
        actions.clickElement("trello.loginPage.loginButton");

        actions.waitForElementVisibleUntilTimeout("trello.atlasian.password",2);
        if (actions.isElementVisible("trello.atlasian.password")) {
        actions.typeValueInField(password, "trello.loginPage.password");
        actions.clickElement("trello.loginPage.loginButton");}
    }
}
