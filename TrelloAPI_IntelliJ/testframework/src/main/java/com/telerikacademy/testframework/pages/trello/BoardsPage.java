package com.telerikacademy.testframework.pages.trello;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class BoardsPage extends BaseTrelloPage {

    public BoardsPage(WebDriver driver) {
        super(driver, "trello.boardsUrl");
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
    }

    public void clickOnBoard(String boardName, String teamName) {
        actions.waitForElementVisibleUntilTimeout("trello.boardsPage.boardByTeamAndName", 30, teamName, boardName);
        actions.clickElement("trello.boardsPage.boardByTeamAndName", teamName, boardName);
    }

}
