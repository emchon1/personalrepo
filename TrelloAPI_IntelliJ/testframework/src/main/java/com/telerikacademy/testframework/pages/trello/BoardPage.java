package com.telerikacademy.testframework.pages.trello;

import com.telerikacademy.testframework.Utils;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class BoardPage extends BaseTrelloPage {

    public BoardPage(WebDriver driver) {
        super(driver, "trello.boardUrl");
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
    }

    public void addCard(String description) {
        actions.waitForElementVisibleUntilTimeout("trello.boardPage.addCard",10);
        actions.clickElement("trello.boardPage.addCard");
        actions.typeValueInField(description + Keys.ENTER, "trello.boardPage.addCardDescription");
        actions.waitForElementPresentUntilTimeout("trello.boardPage.addCardConfirmation", 10, description);
        Utils.LOG.info("Card with description -" + description +"- exists");
    }

    public void dragAndDropCard(String destination) {
        actions.waitFor(2000);
        actions.waitForElementVisibleUntilTimeout("trello.boardPage.dropCardLocation", 10);
        actions.dragAndDropElement("trello.boardPage.dropCardLocation", "trello.boardPage.dropCardDestination",destination);
        Utils.LOG.info("Card drag-and-dropped to " + destination);
    }

    public void closeBoard() {
        actions.waitForElementVisibleUntilTimeout("trello.boardPage.moreMenu", 10);
        actions.clickElement("trello.boardPage.moreMenu");
        actions.waitForElementVisibleUntilTimeout("trello.boardPage.closeBoardButton", 10);
        actions.clickElement("trello.boardPage.closeBoardButton");
        actions.waitForElementVisibleUntilTimeout("trello.boardPage.closeConfirmation", 10);
        actions.clickElement("trello.boardPage.closeConfirmation");
        Utils.LOG.info("Board is closed");
    }

    public void moveCardToList(String cardName, String listName) {

    }

    public void assertListExists(String listName) {
        actions.waitForElementPresentUntilTimeout("trello.boardPage.listByName", 10, listName);
    }

//    public void assertCardExists(String cardDescription) {
//          ASSERTION IS IN THE "add card" method.
//    }

    public void assertCardMoved() {
        actions.waitForElementPresentUntilTimeout("trello.boardPage.moveCardConfirmation", 10);
    }

    public void assertBoardClosed() {
        actions.waitForElementPresentUntilTimeout("trello.boardPage.closeMessage", 10);
    }
}
