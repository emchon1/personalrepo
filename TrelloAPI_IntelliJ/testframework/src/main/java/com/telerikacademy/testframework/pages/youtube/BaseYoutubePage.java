package com.telerikacademy.testframework.pages.youtube;

import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.WebDriver;

public class BaseYoutubePage extends BasePage {
    public BaseYoutubePage(WebDriver driver, String pageUrlKey) {
        super(driver, pageUrlKey);
    }

    public void declineSignInIfAsked() {

        if (actions.isElementPresent("youtube.DeclineSignIn")){
            actions.clickElement("youtube.DeclineSignIn");
        }
    }

    public void acceptConsentPopUp() {
        actions.waitForElementPresentUntilTimeout("popup.IFrame", 30);
        actions.switchToIFrame("popup.IFrame");
        actions.waitForElementPresent("popup.AgreeButton");
        actions.clickElement("popup.AgreeButton");
        actions.getDriver().switchTo().defaultContent();
    }

}
