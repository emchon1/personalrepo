package com.telerikacademy.testframework.trelloapi.Models;

import java.lang.reflect.Array;
import java.util.List;

public class ListModel {
    public String id;
    public String name;
    public boolean closed;
    public String idBoard;
    public String pos;
    public String shortUrl;
    public String url;
    public Object limits;
}

