package com.telerikacademy.testframework.pages.youtube;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class YouTubeVideoPage extends BaseYoutubePage{

    public YouTubeVideoPage(WebDriver driver) {
        super(driver, "youtube.watchUrl");
    }

    public void waitForPageLoad() {
        actions.waitForElementVisible("youtube.watch.Comments");
    }

    public void playVideo() {
        hoverVideo();
        if (actions.isElementPresent("youtube.watch.ads.AdvertiserLink")){
            actions.waitForElementVisible("youtube.watch.ads.SkipButton", 10);
            actions.clickElement("youtube.watch.ads.SkipButton");
        }

        if (actions.isElementVisible("youtube.watch.LargePlayButton")){
            actions.clickElement("youtube.watch.LargePlayButton");
        }

        if(actions.isElementVisible("youtube.watch.PlayButton")){
            actions.pressKey(Keys.SPACE);
        }
    }

    public void pauseVideo() {
        hoverVideo();
        actions.pressKey(Keys.SPACE);
    }

    public void toggleFullscreenMode() {
        hoverVideo();
        actions.waitForElementVisible("youtube.watch.FullscreenButton");
        actions.clickElement("youtube.watch.FullscreenButton");
    }

    public void assertVideoIsPlaying() {
        hoverVideo();
        actions.assertElementAttribute("youtube.watch.PlayPauseButton", "aria-label", "Pause (k)");
    }

    public void assertVideoIsPaused() {
        hoverVideo();
        actions.assertElementAttribute("youtube.watch.PlayPauseButton", "aria-label", "Play (k)");
    }

    public void assertVideoIsInFullScreen() {
        hoverVideo();
        actions.assertElementAttribute("youtube.watch.FullscreenButton", "title", "Exit full screen (f)");
    }

    public void assertVideoIsNotInFullScreen() {
        hoverVideo();
        actions.assertElementAttribute("youtube.watch.FullscreenButton", "title", "Full screen (f)");
    }

    public void watchVideo(int durationSeconds) {
        actions.waitFor(durationSeconds);
    }

    private void hoverVideo() {
        actions.hoverElement("youtube.watch.Video");
    }
}
