package com.telerikacademy.testframework.trelloapi.Models;

import java.util.Date;

public class CardModel {
    public String name;
    public String desc;
    public Object descData;
    public boolean closed;
    public String idBoard;
    public String idList;
    public Object labels;
    public String id;
    public String url;
    public boolean subscribed;
    public Date dateLastActivity;
    public String shortUrl;
    public Object attachments;
}
