package com.telerikacademy.testframework.pages.youtube;

import com.telerikacademy.testframework.Utils;
import org.openqa.selenium.WebDriver;

public class YouTubePage extends BaseYoutubePage{
    public YouTubePage(WebDriver driver) {
        super(driver, "youtube.url");
    }

    public void navigateToPage() {
        Utils.getWebDriver().get(url);
    }

    public void searchForVideo() {
        actions.waitForElementVisible("youtube.SearchField");
        actions.typeValueInField("thriller", "youtube.SearchField");
        actions.clickElement("youtube.SearchButton");
    }

    public void openVideo() {
        actions.waitForElementPresent("youtube.ThrillerSong");
        actions.clickElement("youtube.ThrillerSong");
    }
}
