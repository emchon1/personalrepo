package testCases.youtube;

import org.junit.Test;
import org.openqa.selenium.Keys;
import com.telerikacademy.testframework.pages.youtube.YouTubePage;
import com.telerikacademy.testframework.pages.youtube.YouTubeVideoPage;

public class YouTubeTests extends BaseTest {
    @Test
    public void youTubeVideo_should_startPlaying_when_navigated_and_clickedOnPlay() {

        YouTubePage youtubePage = new YouTubePage(actions.getDriver());

        youtubePage.declineSignInIfAsked();
        youtubePage.acceptConsentPopUp();

        youtubePage.searchForVideo();
        youtubePage.openVideo();

        YouTubeVideoPage youtubeWatchPage = new YouTubeVideoPage(actions.getDriver());
        youtubeWatchPage.assertPageNavigated();
        youtubeWatchPage.waitForPageLoad();

        youtubeWatchPage.declineSignInIfAsked();

        youtubeWatchPage.playVideo();
        youtubeWatchPage.watchVideo(1000);
        youtubeWatchPage.assertVideoIsPlaying();

        youtubeWatchPage.pauseVideo();
        youtubeWatchPage.watchVideo(500);
        youtubeWatchPage.assertVideoIsPaused();

        youtubeWatchPage.toggleFullscreenMode();
        youtubeWatchPage.watchVideo(2000);
        youtubeWatchPage.assertVideoIsInFullScreen();

        actions.pressKey(Keys.ESCAPE);
        youtubeWatchPage.watchVideo(2000);
        youtubeWatchPage.assertVideoIsNotInFullScreen();
    }
}
