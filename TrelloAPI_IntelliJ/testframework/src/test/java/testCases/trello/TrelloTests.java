package testCases.trello;

import com.telerikacademy.testframework.Utils;
import com.telerikacademy.testframework.trelloapi.Models.BoardModel;
import com.telerikacademy.testframework.trelloapi.Models.ListModel;
import com.telerikacademy.testframework.trelloapi.Models.CardModel;
import com.telerikacademy.testframework.trelloapi.TrelloAPI;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.telerikacademy.testframework.pages.trello.BoardPage;
import com.telerikacademy.testframework.pages.trello.BoardsPage;
import com.telerikacademy.testframework.pages.trello.LoginPage;
import org.openqa.selenium.support.ui.Wait;

import javax.swing.*;
import java.util.concurrent.TimeUnit;

public class TrelloTests extends BaseTest {
    private BoardModel createdBoard;
    private ListModel createdList;
    private CardModel createdCard;
    private TrelloAPI trelloAPI;

    @Before
    public void beforeTest(){
        trelloAPI = new TrelloAPI();
        trelloAPI.authenticate("trelloUser");
        createdBoard = trelloAPI.createBoard("Trello Board - " + System.currentTimeMillis(), true);
        Utils.LOG.info("Success");
        createdList = trelloAPI.createList("TestList",createdBoard.id);
        Utils.LOG.info("Success");
        createdCard = trelloAPI.createCard("TestCard" ,createdList.id);
        Utils.LOG.info("Success");
    }

    @Test
    public void a_openExistingBoard_When_BoardNameClicked() {
        LoginPage loginPage = new LoginPage(actions.getDriver());
        loginPage.loginUser("trelloUser");

        BoardsPage boardsPage = new BoardsPage(actions.getDriver());
        boardsPage.clickOnBoard(createdBoard.name, "Personal Boards");

        BoardPage boardPage = new BoardPage(actions.getDriver());
        boardPage.assertListExists("To Do");
    }

    @Test
    public void b_createNewCardInExistingBoard_When_CreateCardClicked() {
        LoginPage loginPage = new LoginPage(actions.getDriver());
        loginPage.loginUser("trelloUser");

        BoardsPage boardsPage = new BoardsPage(actions.getDriver());
        boardsPage.clickOnBoard(createdBoard.name, "Personal Boards");

        BoardPage boardPage = new BoardPage(actions.getDriver());
        boardPage.addCard("DescriptionAdded");
    }
//
    @Test
    public void c_moveCardBetweenStates_When_DragAndDropIsUsed() {
        LoginPage loginPage = new LoginPage(actions.getDriver());
        loginPage.loginUser("trelloUser");

        BoardsPage boardsPage = new BoardsPage(actions.getDriver());
        boardsPage.clickOnBoard(createdBoard.name, "Personal Boards");

        BoardPage boardPage = new BoardPage(actions.getDriver());
//        boardPage.addCard("DescriptionAdded");
        boardPage.dragAndDropCard("Doing");
        boardPage.assertCardMoved();
    }

    @Test
    public void f_deleteBoard_When_DeleteButtonIsClicked() {
        LoginPage loginPage = new LoginPage(actions.getDriver());
        loginPage.loginUser("trelloUser");

        BoardsPage boardsPage = new BoardsPage(actions.getDriver());
        boardsPage.clickOnBoard(createdBoard.name, "Personal Boards");

        BoardPage boardPage = new BoardPage(actions.getDriver());
        boardPage.closeBoard();
    }

    @After
    public void afterTest(){
        trelloAPI.deleteBoard(createdBoard);
    }
}
