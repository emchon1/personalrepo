# Personal Repository Of Emil Nikolov

**Folders:**
* BDD - Java code for Behaviour Driven Design
* JSON_SERVER - jMeter test for local JSON_SERVER
* PostmanHW - Postman Collections and Enviroments for GoRest API and ReqRES API
* SeleniumWebDriver1 - Java code tests of TelerikForum page with Selenium WebDriver
* TrelloAPI - Maven tests on Java of Trello page with Selenium WebDriver and API requests
* Wasp Villas - HTML page by a buddy group in Telerik Academy as a student project
* testframework - Maven tests on Java using the Page model
